const Rx = require('rxjs') ;
const Pipedrive = require('pipedrive');

const pass = 'PasswordPipeline';
const email = 'email';



function saveWithCallbacks(dealId, dataToUpdate, succsessCallback) {
    Pipedrive.authenticate({ email: email, password: pass }, function (error, collectionItems, additionalData) {
        console.log("==================2====")
        if(!error) {
            const api_token = collectionItems[0].api_token;
            const pipedrive = new Pipedrive.Client(api_token, { strictMode: true });

            pipedrive.Deals.get(dealId, function (err, deal) {

                deal.set(dataToUpdate.key,dataToUpdate.value);

                deal.save(function (error, dealSaved, additionalData, req, res) {
                    console.log('error : ' + error);
                    if(!error) succsessCallback(dealSaved);

                })
            });
        }
    });
    console.log("==================1====")
}

// saveWithCallbacks(1, {key:'stage_id', value:4}, result => console.log('dealSaved : ' + JSON.stringify(result, null,2)));



function getPipedriveClient() {
    return new Promise(function (resolve, reject) {
        Pipedrive.authenticate({ email: email, password: pass }, function (error, collectionItems, additionalData) {
            if(error) {
                reject(error);
            }else{
                resolve(new Pipedrive.Client(collectionItems[0].api_token, { strictMode: true }));
            }
        });
    });
}

function getDeal(client, dealId) {
    return new Promise((resolve, reject)=>{
        client.Deals.get(dealId, function (err, deal) {
            resolve(deal);
        });
    });
}

function saveDeal(deal, dataToUpdate) {
    return new Promise((resolve, reject)=>{
        deal.set(dataToUpdate.key,dataToUpdate.value);
        deal.save(function (error, dealSaved, additionalData, req, res) {
            console.log('error : ' + error);
            if(!error) resolve(dealSaved);
        })
    });

}

function saveWithPromise() {
    getPipedriveClient()
        .then(client=>getDeal(client, 1))
        .then(deal => saveDeal(deal, {key:'stage_id', value:1}))
        .then(result => console.log('result : ' + JSON.stringify(result, null,2)))
}


// saveWithPromise();


function listenDeals() {

    const observable = Rx.Observable.create((observer)=>{
        getPipedriveClient()
            .then(client => {
                client.on('deal.updated', function(event, data) {
                    observer.next(data.current);
                });
            });
    });

    observable
        .filter(nextValue => nextValue.id === 2)
        .map(nextValue => ({id:nextValue.id, title: nextValue.title, stage_id: nextValue.stage_id}))
        .scan((acc, nextValue)=>{
            acc.push(nextValue);
            return acc;
        },[])
        .subscribe(
        (nextValue)=> console.log('valor', nextValue),
        (error) => console.log('error', error),
        ()=> console.log(' Cuando se completa!')
    );
}

listenDeals();






