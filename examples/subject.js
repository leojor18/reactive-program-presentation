"use strict";
const Rx = require('rxjs') ;




const subject = new Rx.Subject();

let i=0;
setInterval(()=>{
    subject.next(i++);
}, 1000);

const subscriber = subject
    .filter(value=>value%2===0)
    .subscribe(
        (nextValue)=> console.log('valor', nextValue),
        (error) => console.log('error', error),
        ()=> console.log(' Cuando se completa!')
    );

// setTimeout(()=>subscriber.unsubscribe(),5000);