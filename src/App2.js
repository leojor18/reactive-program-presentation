import React, { Component } from 'react';
import Rx from 'rxjs';
import logo from './logo.svg';
import './App.css';


const subjectInput1 = new Rx.Subject();
const subjectInput2 = new Rx.Subject();
const result = subjectInput1.combineLatest(subjectInput2, (a,b)=>Number(a)*Number(b));


class App extends Component {
    constructor(props){
        super(props);
        this.state ={searchResult:[]};
    }
    componentDidMount(){
        result.subscribe(result => this.setState({result}))
    }
  render() {
    return (
      <div className="App" style={divStyle}>
        <div className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h2>Welcome to React</h2>
        </div>
        <p className="App-intro">
          To get started, edit <code>src/App.js</code> and save to reload.
        </p>
          <div className="inputs">
              <label for="input1">Input 1: </label>
              <input id="input1" onChange={(e)=>subjectInput1.next(e.target.value)} type="text"/>
              <br/>
              <label for="input2">Input 2: </label>
              <input id="input2" onChange={(e)=>subjectInput2.next(e.target.value)} type="text"/>
              <p><b>Result:  </b>{this.state.result}</p>
          </div>
      </div>
    );
  }
}

const divStyle = {
    "font-size" : 24
};


export default App;
