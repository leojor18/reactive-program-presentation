import React, { Component } from 'react';
import Rx from 'rxjs';
import logo from './logo.svg';
import './App.css';
const API_HOME ='http://localhost:3000/ptint/vehicles?openTextSearch=';
function fetchSearch(query) {
    return fetch(`${API_HOME}${query}`, {
        header: { method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Origin': ''
            }
        }})
        .then(response => response.json()).then(response => response.result_set)
}


const subjectInputSearch = new Rx.Subject();

const resultSearch = subjectInputSearch
    .debounceTime(1000)
    .flatMap(query => fetchSearch(query));

class App extends Component {
    constructor(props){
        super(props);
        this.state ={searchResult:[]};
    }
    componentDidMount(){
        resultSearch.subscribe(searchResult => this.setState({searchResult}));
    }
  render() {
    return (
      <div className="App">
        <div className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h2>Welcome to React</h2>
        </div>
        <p className="App-intro">
          To get started, edit <code>src/App.js</code> and save to reload.
        </p>
          <label>Search: </label>
          <input className="search" onChange={(e)=>subjectInputSearch.next(e.target.value)} placeholder="Open text search" type="text"/>
          <div className="container-result">
              <ul>
              {this.state.searchResult.map((result, i)=><li key={i}> {`${result.brand} ${result.model} ${result.subtype} ${result.city}`}</li>)}
            </ul>
          </div>
      </div>
    );
  }
}

export default App;
